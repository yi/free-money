# Internationalization settings
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = str(os.getenv('LANGUAGE'))

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_TZ = True

USE_THOUSAND_SEPARATOR = True
NUMBER_GROUPING = 3
DECIMAL_SEPARATOR = ','

from django.utils.translation import gettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
)