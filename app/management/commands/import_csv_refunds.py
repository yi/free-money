"""
Command to import users who already received a refund

CSV header:
filename of a csv file which contain matriculations_numbers and a field
"fullrefund".
"""
from django.core.management.base import BaseCommand
import pandas as pd

from app.models import Studi


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('filename', nargs=1, type=str)

    def handle(self, *args, **options):
        filename = options['filename'][0]
        dbframe = pd.read_csv(filename, encoding='utf-8')
        for dbframe in dbframe.itertuples():
            obj, created = Studi.objects.update_or_create(
                matriculation_number=dbframe.matrikelnummer,
                defaults={
                    'fullrefund': dbframe.fullrefund
                })
            obj.save()
            print('created' if created else 'updated', obj)
        print('done')
