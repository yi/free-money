"""
Views for 9euro RESST


"""
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.template.loader import get_template
from django.views.generic.base import ContextMixin
from django.views.generic.edit import UpdateView
from ratelimit.decorators import ratelimit

from ipware import get_client_ip

from .models import Studi


def index(request):
    """
    Entry page.
    """
    # sent user info to context if authenticated
    context= {
        'amount_medtec': settings.SEPA_VARAMOUNT,
        'amount_std': settings.SEPA_STDAMOUNT,
        'deadline': settings.DEADLINE,
    }
    if request.user.is_authenticated:
        context.update({
            'full_name': request.user.get_full_name(),
            'first_name': request.user.get_short_name(),
            'matriculation_number': request.user.get_username(),
        })
    return render(request, 'app/index.html', context)


# FIXME refactor to more descriptive identifier
def send(subject, msg, recipient):
    """
    Shortcut to send e-mail from service address.

    :param subject: E-mail subject
    :param msg: Message content
    :param recipient: Recipient address
    """
    send_mail(
        subject,
        msg,
        settings.SERVICE_EMAIL,
        [recipient]
    )

@method_decorator(ratelimit(key='ip', rate='4/m', method='GET', block=True), name='get')
@method_decorator(ratelimit(key='post:username', rate='2/m', method='POST', block=True), name='post')
class StudiUpdateView(UpdateView, ContextMixin, LoginRequiredMixin):
    """
    If authenticated, allow user to update their iban data.
    Class is ratelimited - only 2 post-requests per minute per user allowed.
    """
    login_url = '/saml2/login'
    model = Studi
    fields = ['ac_owner', 'iban', 'bic']
    template_name_suffix = '_update_form'

    def get(self, *args, **kwargs):
        pass
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect('/saml2/login')
        elif self.request.user.is_staff:
            return HttpResponseRedirect('/sepa')
        # After succesful login we need to check if the student's matriculation
        # number exists in our database. If not, the student is rejected because
        # they are either not a student or were not enrolled at the time.
        try:
            self.get_object()
        except Studi.DoesNotExist:
            return render(
                self.request,
                'app/reject.html',
                {
                    'first_name': self.request.user.get_short_name(),
                    'full_name': self.request.user.get_full_name(),
                    'matriculation_number': self.request.user.get_username(),
                    'reason': _('no_student_rejection')
                }
            )
        return super().get(*args, **kwargs)

    def form_valid(self, form):
        """
        Set user and ip for history (def save() in model)
        """
        form.instance.user = self.request.user
        form.instance.ip = get_client_ip(self.request)
        studi = self.get_object()
        # before writing to db, check if sepa export occured in the meantime
        if studi.sepa_file is not None:
            # sepa export occured -> redirect user to detail page and don't send new data to db
            return redirect('details')
        # get count of how many times the newly received iban has been used
        count = Studi.objects.filter(iban=form.cleaned_data['iban']).count()
        # if threshold is set and above count exceeds threshold -> send warning mail
        if int(settings.WARNING_IBAN_USED) > 0 and int(settings.WARNING_IBAN_USED) < count:
            mail_text = get_template('app/mail_iban_warning.txt')
            mail_context = {
                'iban': form.cleaned_data['iban'],
                'count': count
            }
            mail_content = mail_text.render(mail_context)
            subject = _('IBAN Warnung')
            send(subject, mail_content, settings.WARNING_IBAN_MAIL)
        return super().form_valid(form)

    def get_object(self):
        return Studi.objects.get(pk=self.request.user.get_username())

    def get_success_url(self):
        studi = self.get_object()
        user_mail = self.request.user.email
        # send a confirmation to the user if he has a mailadress
        if user_mail:
            studi.email = user_mail
            mail_text = get_template('app/mail_success.txt')
            mail_context = {
                'first_name': studi.first_name,
                'short_iban': studi.iban[-4:]
            }
            mail_content = mail_text.render(mail_context)
            subject = _('mail_subject_success')
            send(subject, mail_content, user_mail)
        return reverse('finished')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        rejection = None
        studi = self.get_object()

        # check if studi already received a refund
        if studi.fullrefund:
            rejection = 'other_reasons'
        if studi.sepa_file is not None:
            rejection = 'already_refunded'

        # sent context to template if user has no mail (to warn him)
        if not self.request.user.email:
            context['nomail'] = True

        # get amount of money the student will receive based on settings and their course of study
        amount = settings.SEPA_STDAMOUNT
        if studi.medizintechnik:
            amount = settings.SEPA_VARAMOUNT

        context['rejection'] = rejection
        context['amount'] = amount
        context['first_name'] = self.request.user.get_short_name()
        context['full_name'] = self.request.user.get_full_name()
        context['matriculation_number'] = self.request.user.get_username()
        context['update_instead_of_create'] = studi.iban is not None
        return context

@login_required
def finished(request):
    """
    Final page where user gets their data shown and may go back to correct some things.
    """
    studi = Studi.objects.get(pk=request.user.get_username())

    assert studi.ac_owner is not None
    assert studi.iban is not None
    assert studi.bic is not None

    context = {
        'studi': studi,
        'full_name': request.user.get_full_name(),
        'matriculation_number': request.user.get_username(),
        'email': request.user.email,
    }
    return render(request, 'app/finished.html', context)


def denied(request):
    """
    "Denied" page for users whose accounts do not contain a matriculation number.
    """
    return render(request, 'app/denied.html', status=403)


def failure(request, exception=None, status=403, **kwargs):
    """
    Failure page - used by SAML auth.

    Redirects to denied due to i18n is not usable on the error page
    """
    return redirect(denied)

def user_logout(request):
    logout(request)
    return render(request, 'app/logout.html')

@login_required
def ratelimit(request, exception=None, status=403, **kwargs):
    """
    Show error page if student runs into rate limiting in StudiUpdateView.
    """
    studi = Studi.objects.get(pk=request.user.get_username())
    context = {
        'studi': studi,
        'full_name': request.user.get_full_name(),
        'first_name': request.user.get_short_name(),
        'matriculation_number': request.user.get_username(),
    }
    return render(request, 'app/ratelimit.html', context)
